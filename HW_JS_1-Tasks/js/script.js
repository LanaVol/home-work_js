// Задача 1. По координатам двух точек, которые вводит пользователь, определить уравнение прямой, проходящей через эти точки.
//   Общий вид уравнения: y = kx + b; где k = (y1 - y2) / (x1 - x2), b = y2 - k*x2.

var x1 = +prompt("Введіть будь ласка координатe x1 для першої точки?");
var y1 = +prompt("Введіть будь ласка координатe y1 для першої точки?");
var x2 = +prompt("Введіть будь ласка координатe x2 для другої точки?");
var y2 = +prompt("Введіть будь ласка координатe y2 для другої точки?");
var k = (y1 - y2) / (x1 - x2);
var b = y2 - k * x2;

document.write(
  `Рівняння прямої, що проходить через задані координати двох точок А(${x1}, ${y1}) та В(${x2}, ${y2}):<br> 
  y = ${k}x + ${b};<br> 
  де k = (y1 - y2) / (x1 - x2), b = y2 - k*x2.)`
);

// Задача 2. Обменять значения двух переменных, используя третью (буферную) переменную.

var firstBox = 2;
var secondBox = 5;
var bufferBox;

console.log(
  `firstBox містить: ${firstBox}, secondBox містить: ${secondBox}. Обміняємо значення за допомогою bufferBox.`
);

bufferBox = firstBox;
firstBox = secondBox;
secondBox = bufferBox;

console.log(
  `Тепер firstBox містить: ${firstBox}, secondBox містить: ${secondBox}.`
);


//  Задача 3. Объявите две переменные: admin и name. Запишите в name строку "Василий". Замените значение из name в admin. Выведите admin.

var admin;
var name = "Василий";

admin = name;
console.log(admin);


// Задача 4. Получить от пользователя данные о пользователе: имя, фамилию, возраст. Обработать их и вывести на экран.

var firstName = prompt("Please enter your first name:");
var lastName = prompt("Please enter your last name:");
var age = +prompt("Please enter your age:");

document.write("Hello!<br>");
document.write(
  "Your first name is: " +
    firstName +
    " and  your last name is: " +
    lastName +
    ".<br>"
);
document.write("You are " + age + " years old.");


// Задача 5. Запросите у пользователя его возраст, после чего выведите на экран модальное окно с вопросом: "Ваш возраст ...(введеное пользователем число) лет?". Далее в зависимости от выбора пользователя выводится модальное окно с информацией true или false.

var age = +prompt("Введіть будь ласка ваш вік?");
var question = confirm(`Ваш вік: ${age} років?`);
var answer = alert(`Відповідь: ${question}`);

