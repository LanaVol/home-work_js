const field = document.querySelector(".field");
const ball = document.querySelector(".field__ball");
const goal1 = document.querySelector(".goal1");
const goal2 = document.querySelector(".goal2");
const team1 = document.querySelector(".count__team1");
const team2 = document.querySelector(".count__team2");
const subtitle1 = document.querySelector(".count__subtitle1");
const subtitle2 = document.querySelector(".count__subtitle2");

let counterTeam1 = 0;
let counterTeam2 = 0;

field.addEventListener("dragover", preventDefault);
field.addEventListener("drop", dropBallToField);
goal1.addEventListener("drop", getCountValue);
goal2.addEventListener("drop", getCountValue);

function preventDefault(e) {
  e.preventDefault();
}

function dropBallToField(e) {
  // console.log(e.clientX);
  // console.log(e.currentTarget);
  // console.log(e.currentTarget.offsetLeft);
  // console.log(e.offsetX);
  ball.style.left = e.clientX - e.currentTarget.offsetLeft - 10 + "px";
  ball.style.top = e.clientY - e.currentTarget.offsetTop - 10 + "px";

  this.appendChild(ball);
}

function getCountValue(e) {
  if (e.target.dataset.name == "goal1") {
    counterTeam1 += 1;
    team1.innerHTML = counterTeam1;
  } else if (e.target.dataset.name == "goal2") {
    counterTeam2 += 1;
    team2.innerHTML = counterTeam2;
  }
  colorWinCount();
}

function colorWinCount() {
  if (counterTeam1 > counterTeam2) {
    subtitle1.classList.add("countGoal");
  } else if (counterTeam1 < counterTeam2) {
    subtitle2.classList.add("countGoal");
  } else {
    subtitle1.classList.remove("countGoal");
    subtitle2.classList.remove("countGoal");
  }
}
