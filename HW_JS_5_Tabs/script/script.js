var tabs = document.querySelectorAll(".tabs__tab");
var content = document.querySelectorAll(".tabs__p");

document
  .querySelector(".tabs__container")
  .addEventListener("click", showTabContent);

function showTabContent(e) {
  tabs.forEach((element, index) => {
    element.classList.remove("active");
    content[index].classList.add("hide");
    if (e.target === element) {
      content[index].classList.remove("hide");
    }
  });
  e.target.classList.add("active");
}

// ************************************Варіант 2(для себе)
// var elemId; //додаткова змінна для варіанту 2
// document
//   .querySelector(".tabs__container")
//   .addEventListener("click", showTabContent);

// function showTabContent(e) {
//   tabs.forEach((element, index) => {
//     element.classList.remove("active");
//     if (e.target === element) {
//       elemId = index;
//     }
//   });
//   e.target.classList.add("active");
//   content.forEach((item, index) => {
//     item.classList.add("hide");
//     if (elemId === index) {
//       item.classList.remove("hide");
//     }
//   });
// }
