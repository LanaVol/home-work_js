const regions = document.querySelectorAll("path[data-region]");

//   створюю назви регіонів, задаю їм клас hide і при відповідних подіях - перемикаю клас hide
regions.forEach(function (el) {
  const tooltip = document.createElement("div");
  const triangle = document.createElement("div");

  tooltip.classList.add("tooltip");
  triangle.classList.add("triangle");

  tooltip.classList.add("hide");
  triangle.classList.add("hide");
  tooltip.innerHTML = el.getAttribute("data-region");
  tooltip.style.top = el.nextElementSibling.getAttribute("cy") - 40 + "px";
  tooltip.style.left = el.nextElementSibling.getAttribute("cx") - 15 + "px";

  triangle.style.top = el.nextElementSibling.getAttribute("cy") - 12 + "px";
  triangle.style.left = el.nextElementSibling.getAttribute("cx") - 2 + "px";

  document.body.appendChild(tooltip);
  document.body.appendChild(triangle);
  console.log(tooltip);

  el.addEventListener("mouseover", function () {
    tooltip.classList.toggle("hide");
    triangle.classList.toggle("hide");
  });
  el.addEventListener("mouseleave", function () {
    tooltip.classList.toggle("hide");
    triangle.classList.toggle("hide");
  });
});
