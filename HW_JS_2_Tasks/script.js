// Завдання 1. Среди трех чисел найти среднее арифметическое. Если среди чисел есть равные, вывести сообщение "Ошибка". Числа принять от пользователя.

var num1 = prompt("Введіть перше число:");
var num2 = prompt("Введіть друге число:");
var num3 = prompt("Введіть третє число:");
console.log("Перше: " + num1);
console.log("Друге: " + num2);
console.log("Третє: " + num3);

if (num1 === null && num2 === null && num3 === null) {
  console.log("Ви не ввели жодного числа"); // у всіх випадках натиснуто "відміна"
}
else if (num1 === null || num2 === null || num3 === null) {
  console.log("Ви не ввели одне з чисел"); // в одному з випадків натиснуто "відміна"
}
else if (num1 === "" && num2 === "" && num3 === "") {
  console.log("Для обчислення результату потрібно ввести числа"); // у всіх випадках натиснуто ОК без введеного числа
}
else if (num1 === "" || num2 === "" || num3 === "") {
  console.log("Для обчислення результату потрібно ввести три числа"); // в одному з випадків відсутнє число і натиснуто ОК
}
else if (num1 == num2 || num2 == num3 || num1 == num3) {
  console.log("ПОМИЛКА! Ви ввели однакові числа"); // якщо серед введених чисел э рівні
}
else {
  var result = (+num1 + +num2 + +num3) / 3;
  console.log("Середнє арифметичне значення: " + result);
}

// Завдання 2. Построить прямоугольный треугольник состоящий из символа "*".

var box = "";

for (var i = 0; i <= 10; i++) {
  box += "*";
  console.log(box);
}

//Завдання 3. Определить какое из трех, введенных пользователем, чисел максимальное и вывести его на экран.
var num1 = +prompt("Введіть перше число");
var num2 = +prompt("Введіть друге число");
var num3 = +prompt("Введіть третє число");

console.log("Перше: " + num1);
console.log("Друге: " + num2);
console.log("Третє: " + num3);
var maxNum;

if(num1 == num2 || num2 == num3 || num1 == num3) {
 document.write("Ви ввели однакові числа");
}
else if(num1 > num2 && num1 > num3) {
 maxNum = num1;
 document.write("Максимальне число: " + maxNum);
} 
else if(num2 > num1 && num2 > num3) {
 maxNum = num2;
 document.write("Максимальне число: " + maxNum);
}
else if(num3 > num2 && num3 > num1) {
 maxNum = num3;
 document.write("Максимальне число: " + maxNum);
}
else {
 document.write("Максимальне число не знайдено");
}

// Завдання 4. Дано два числа A и B где (A<B). Выведите на экран суму всех чисел расположенных в числовом промежутке от А до В. Выведите на экран все нечетные значения, расположенные в числовом промежутке от А до В. 

var a = +prompt("А?");
var b = +prompt("В?");

var sum = 0;
var oddNum;
document.write(`Непарнi числа в проміжку від ${a} до ${b} (не включно!): <br>`);

// числа в проміжку від А до В не включно; якщо А і В включно, то в циклі (let i=a; i<=b; i++)
   
for (let i = a+1; i < b; i++) {
  sum += i; 
  if (i % 2 != 0) {
     oddNum = i;
     document.write(`Непарне: ${oddNum} <br>`);
  }
}
document.write(`Сумма всіх чисел в проміжку між ${a} до ${b} (не включно!): ${sum}<br>`);


// 5. Вывести на экран ряд чисел Фибоначчи, состоящий из n элементов (n принять от пользователя).
// Числа Фибоначчи – это элементы числовой последовательности, в которой каждое последующее число равно сумме двух предыдущих. 0, 1(), 1(0+1), 2(1+1), 3(2+1), 5(2+3), 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987

var n = +prompt("Введіть число n - кількість елементів:");
var firstNum = 0;
var secondNum = 1;
var previousNum = firstNum + secondNum;  
var currentNum = secondNum + previousNum;  
var nextNum;

document.write("Ряд чисел Фібоначчі: <br>");
document.write(firstNum);
document.write(", " + secondNum);
document.write(", " + previousNum);
document.write(", " + currentNum);

for (let i=1; i <= n; i++) {
  nextNum = currentNum + previousNum;
  previousNum = currentNum;
  currentNum = nextNum;
   
  document.write(", " + nextNum);
}


// Завдання 6. Вывести на экран таблицу умножения от 0 до 9. 
var operand1 = 0;
var operand2 = 0;
var result;
   
for (let i=0; i<10; i++) {
  operand1 = i;
  for (let i=1; i<10; i++) {
     operand2 = i;
     result = operand1 * operand2;
     document.write(`Результат ${operand1} * ${operand2} = ${result} <br>`);
  }
  document.write("***<br>");
}
