const testTimer = document.querySelector(".test-timer");

let sec = 0;
let min = 0;
let timerToMinute = setInterval(() => {
  if (min == 40) {
    return;
  } else if (sec < 9 && min <= 9) {
    testTimer.innerHTML = `0${min} : 0${++sec}`;
  } else if (sec < 9 && min >= 9) {
    testTimer.innerHTML = `${min} : 0${++sec}`;
  } else if (sec == 59 && min <= 9) {
    min += 1;
    sec = 0;
    testTimer.innerHTML = `0${min} : 0${sec}`;
  } else if (sec == 59 && min >= 9) {
    min += 1;
    sec = 0;
    testTimer.innerHTML = `${min} : 0${sec}`;
  } else if (sec >= 9 && min <= 9) {
    testTimer.innerHTML = `0${min} : ${++sec}`;
  } else {
    testTimer.innerHTML = `${min} : ${++sec}`;
  }
}, 1000);
