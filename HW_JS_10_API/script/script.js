let data;
fetch("https://dummyjson.com/products")
  .then((res) => res.json())
  .then((json) => {
    data = json;
    showData();
  });

const smartphones = document.querySelector(".product__smartphones");
const laptops = document.querySelector(".product__laptops");

function showData() {
  const cardsSmartphone = data.products
    .map((el) => (el.category === "smartphones" ? createEl(el) : ""))
    .join("");
  smartphones.insertAdjacentHTML("afterbegin", cardsSmartphone);

  const cardsLaptops = data.products
    .map((el) => (el.category === "laptops" ? createEl(el) : ""))
    .join("");
  laptops.insertAdjacentHTML("afterbegin", cardsLaptops);
}

function createEl({ category, images, title, description, price }) {
  return `<div class="product__card">
                  <div class="product__card-img"><img src="${images[0]}" alt="Product image"  /></div>
                  <div class='product__card-info'><div class="product__card-title">${title}</div>
                    <div class="product__card-description">${description}</div>
                    <div class="product__card-price">$${price}</div></div>
                </div>`;
}
