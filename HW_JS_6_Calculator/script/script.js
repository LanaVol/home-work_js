var number = document.querySelectorAll(".number");
var sign = document.querySelectorAll(".sign");
var equally = document.querySelector(".equally");
var input = document.querySelector(".calc__inp");
var clear = document.querySelector(".clear");
var point = document.querySelector(".point");
var minus = document.querySelector(".minus");
var percent = document.querySelector(".percent");
var op1 = "";
var opSign = "";
var op2 = "";
var result = "";

document.querySelector(".calc__box").addEventListener("click", showEventValue);
function showEventValue(e) {
  // по кліку вмикаємо відповідні функції залежно від класу
  if (e.target.classList.contains("number")) {
    showInputValue(e);
  } else if (e.target.classList.contains("sign")) {
    showSignValue(e);
  } else if (e.target.classList.contains("equally")) {
    calcResult();
  } else if (e.target.classList.contains("clear")) {
    clearAll();
  } else if (e.target.classList.contains("percent")) {
    showSignValue(e);
  } else if (e.target.classList.contains("minus")) {
    toggleMinus();
  } else if (e.target.classList.contains("point")) {
    addPoint(e);
  } else {
    return;
  }
}
// ----------функція при кліку на число ----------------
function showInputValue(e) {
  if (result !== "") {
    result = "";
    clearAll();
    console.log(e.target.innerText);
  }

  input.value += e.target.innerText;

  // присвоєння до першого операнду
  if (opSign === "" && op2 === "") {
    op1 = input.value;
    console.log(`ор1: ${op1}`);
  }
  // присвоєння до другого операнду
  if (opSign !== "" && op1 !== "") {
    op2 += e.target.innerText;
    console.log(`ор2: ${op2}`);
  }
}

// ----------функція при кліку на знак ----------------
function showSignValue(e) {
  if (!op1) {
    return;
  } else if (result !== "") {
    op1 = result;
    op2 = "";
    opSign = "";
    result = "";
  } else if (opSign !== "") {
    console.log("Помилка! Знак введено");
    return;
  }
  input.value += e.target.innerText;

  if (op1 !== "" && op2 === "") {
    opSign = e.target.innerText;
  }
}

// ----------функція при кліку на ""="" ----------------
function calcResult() {
  switch (opSign) {
    case "+":
      result = +op1 + +op2;
      break;
    case "-":
      result = op1 - op2;
      break;
    case "*":
      result = op1 * op2;
      break;
    case "/":
      result = op1 / op2;
      break;
    case "%":
      result = op1 % op2;
      break;
  }
  input.value = result;
}

// ----------функція при кліку на "A/C" ----------------
function clearAll() {
  input.value = "";
  op1 = "";
  opSign = "";
  op2 = "";
}

// ----------функція при кліку на "+/-"" ----------------
function toggleMinus() {
  if (op1 !== "" && opSign === "" && op2 === "") {
    op1 *= -1;
    input.value = op1;
  } else if (result !== "") {
    result *= -1;
    op1 = result;
    input.value = op1;
  } else if (op1 !== "" && opSign !== "" && op2 !== "") {
    op2 *= -1;
    input.value = `${op1}${opSign}(${op2})`;
  } else {
    return;
  }
}

// ----------функція при кліку на "." ----------------
function addPoint(e) {
  if (result !== "") {
    result = "";
    clearAll();
    return;
  } else if (!op1) {
    return;
  } else if (op1 && opSign && !op2) {
    return;
  } else if (op1.includes(".") && opSign === "") {
    return;
  } else if (op1 !== "" && op2.includes(".")) {
    return;
  }
  input.value += e.target.innerText;
  // присвоєння до першого операнду
  if (opSign !== "" && op2 === "") {
    op1 = input.value;
  }
  // присвоєння до другого операнду
  else if (opSign !== "" && op1 !== "" && op2 !== "") {
    op2 += e.target.innerText;
  }
}

// keydown на '=' і 'A/C'
document.body.addEventListener("keydown", doFromKeydown);
function doFromKeydown(e) {
  if (e.code === "Enter") {
    calcResult();
  } else if (e.code === "Escape") {
    clearAll();
  }
}
