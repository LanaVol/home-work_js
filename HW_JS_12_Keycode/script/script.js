const key = document.querySelector(".key");
const code = document.querySelector(".code");
const which = document.querySelector(".which");
const keyWhich1 = document.querySelector(".keyWhich1");
const keyWhich2 = document.querySelector(".keyWhich2");
const history = document.querySelector(".history");

let arr = [];

window.addEventListener("keydown", getInfoKeycodeEvent);

function getInfoKeycodeEvent(event) {
  key.innerHTML = event.key;
  code.innerHTML = event.code;
  which.innerHTML = event.which;
  keyWhich1.innerHTML = event.which;
  keyWhich2.innerHTML = event.which;
  showHistoryKey(event);
}

function showHistoryKey(event) {
  Array.from(history.children).forEach((child) => {
    if (
      history.children.length !== 0 &&
      child.innerHTML === event.key.toUpperCase()
    ) {
      child.remove();
      let index = arr.indexOf(event.key.toUpperCase());
      arr.splice(index, 1);
    }
  });

  let currentKey = event.key.toUpperCase();
  arr.unshift(currentKey); //додаю в початок масиву поточну клавішу

  let li = document.createElement("li");
  li.innerHTML = arr[0];
  history.insertAdjacentElement("afterbegin", li); //додаю li в верстку

  let clickedKey = event;
  li.addEventListener("click", showInfoKeyFromClick);

  function showInfoKeyFromClick() {
    key.innerHTML = clickedKey.key;
    code.innerHTML = clickedKey.code;
    which.innerHTML = clickedKey.which;
    keyWhich1.innerHTML = clickedKey.which;
    keyWhich2.innerHTML = clickedKey.which;
  }

  if (history.children.length > 4) {
    history.lastElementChild.remove();
  }
}
