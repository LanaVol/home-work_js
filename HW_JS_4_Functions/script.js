// 1. Создать функцию calculate(operand1, operand2, sign), где operand1 и operand2 — два числа, sign — знак арифметической операции.
//  Функция должна расчитывать результат операции, исходя из переданного ей знака.
//  Функция должна проверять корректность введенных чисел и знака операции.
//  Все аргументы для функции принять от пользователя.

function calculate(operand1, operand2, sign) {
  var firstOperand = prompt("Введіть перше число: ");
  var sign = prompt("Введіть знак дії: +, -, *, /");
  var secondOperand = prompt("Введіть друге число: ");

  // перевірка на випадок коли не введено дані і нажато на ОК або ВІДМІНА
  while (
    firstOperand === null ||
    secondOperand === null ||
    firstOperand === "" ||
    secondOperand === ""
  ) {
    firstOperand = prompt("Дані не введено. Введіть заново перше число: ");
    secondOperand = prompt("Дані не введено. Введіть заново друге число: ");
  }

  // приводимо в числовий тип
  var operand1 = +firstOperand;
  var operand2 = +secondOperand;
  var result;

  // перевірка операндів на число
  while (isNaN(operand1) || isNaN(operand2)) {
    if (isNaN(operand1)) {
      operand1 = prompt(
        "Дані введено не коректно. Введіть заново перше число: "
      );
    } else {
      operand2 = prompt(
        "Дані введено не коректно. Введіть заново друге число: "
      );
    }
  }

  switch (sign) {
    case null:
      sign = prompt("Ви нажали ВІДМІНА. Введіть знак дії: +, -, *, /");
      break;
    case "":
      sign = prompt("Ви нажали ОК. Введіть знак дії: +, -, *, /");
      break;
    case "+":
      result = operand1 + operand2;
      break;
    case "-":
      result = operand1 - operand2;
      break;
    case "*":
      result = operand1 * operand2;
      break;
    case "/":
      result = operand1 / operand2;
      break;
    default:
      document.write("Введений знак дії не коректний.<br>");
  }

  return document.write(
    `Перше значення: ${operand1}.<br>Друге значеня: ${operand2}.<br> Результат: ${operand1} ${sign} ${operand2} = ${result}<br>`
  );
}

calculate();

console.log("****************************************");

// 2. Создать функцию, возводящую число в степень, число и сама степень вводится пользователем
var num = prompt("Введіть число:");
var powNum = prompt("Введіть степінь, починаючи від 1:");

// перевірка на випадок не введених даних і натиснутих ОК або Відміна
while (num === null || num === "" || powNum === null || powNum === "") {
  if (num === null || powNum === "") {
    num = prompt("Число не введено. Введіть число:");
  }
  powNum = prompt("Степінь не введено. Введіть степінь, починаючи від 1:");
}

// перевірка на число
while (isNaN(num) || isNaN(powNum)) {
  if (isNaN(num)) {
    num = prompt("Ви ввели не числове значення. Введіть заново число:");
  }
  powNum = prompt(
    "Ви ввели не числове значення степені. Введіть заново степінь, починаючи від 1:"
  );
}

var res = calcToPow(num, powNum);

function calcToPow(num, powNum) {
  var result = num;
  for (let i = 2; i <= powNum; i++) {
    result = result * num;
  }
  return result;
}

console.log(`Число ${num} піднесене до ${powNum} степеню буде: ${res}`);
console.log("****************************************");

// 3.  Создать игру "Камень-Ножницы-Бумага".
//     Выбор компьютера определяется строкой:
//     "var computerChoice = Math.random();
//     alert(computerChoice);".
//     Math.random() выдает произвольное число в промежутке от 0 до 1, на основании этого можно построить принцип выбора решения компьютера.

//-----------------------!!!!!!!!!!!ВАРІАНТ 1+HTML!!!!!!!!!-----------------------
var computerChoice = Math.round(Math.random() * 10);
var rock = [0, 1, 2, 3]; // "Камінь"
var scissors = [4, 5, 6]; //"Ножиці"
var paper = [7, 8, 9, 10]; //"Папір"

console.log(computerChoice);

//перебір варіантів вибор computerChoice i переприсвоєння computerChoise
if (rock.includes(computerChoice)) {
  computerChoice = "Камінь";
} else if (scissors.includes(computerChoice)) {
  computerChoice = "Ножиці";
} else {
  computerChoice = "Папір";
}

// порівняння виборів
function checkOnClick() {
  if (computerChoice == userChoise) {
    document.write(
      `<div style='color:orange; text-align:center;'><h1>НІЧИЯ!</h1>Вибори співпали: <br> ${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Камінь" && userChoise == "Ножиці") {
    document.write(
      `<div style='color:black; text-align:center;'><h1>Ви програли!</h1><br>Камінь ламає ножиці:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Камінь" && userChoise == "Папір") {
    document.write(
      `<div style='color:green; text-align:center;'><h1>УРА! Ви виграли!</h1><br>Камінь накриває папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Ножиці" && userChoise == "Камінь") {
    document.write(
      `<div style='color:green; text-align:center;'><h1>УРА! Ви виграли!</h1><br>Камінь ламає ножиці:<br>${computerChoice} \& ${userChoise}<div>`
    );
  } else if (computerChoice == "Ножиці" && userChoise == "Папір") {
    document.write(
      `<div style='color:black; text-align:center;'><h1>Ви програли!</h1><br>Ножиці ріжуть папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Папір" && userChoise == "Камінь") {
    document.write(
      `<div style='color:black; text-align:center;'><h1>Ви програли!</h1><br>Камінь накриває папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Папір" && userChoise == "Ножиці") {
    document.write(
      `<div style='color:green; text-align:center;'><h1>УРА! Ви виграли!</h1><br>Ножиці ріжуть папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  }
}

var userChoise;
var rockBtn = document.getElementById("rockBtn");
rockBtn.addEventListener("click", function () {
  console.log(`Мій вибір Камінь`);
  userChoise = "Камінь";
  checkOnClick();
});
var scissorsBtn = document.getElementById("scissorsBtn");
scissorsBtn.addEventListener("click", function () {
  console.log(`Мій вибір Ножиці`);
  userChoise = "Ножиці";
  checkOnClick();
});
var paperBtn = document.getElementById("paperBtn");
paperBtn.addEventListener("click", function () {
  console.log(`Мій вибір Папір`);
  userChoise = "Папір";
  checkOnClick();
});

console.log(`Комп'ютер обрав: ${computerChoice}`);
console.log("****************************************");

// -------------------ВАРІАНТ 2!!!! виконання через PROMPT--------------------------
var computerChoice = Math.round(Math.random() * 10);

var rock = [0, 1, 2, 3]; // "Камінь"
var scissors = [4, 5, 6]; //"Ножиці"
var paper = [7, 8, 9, 10]; //"Папір"

console.log(computerChoice);

//перебір варіантів вибору computerChoice i переприсвоєння значення в computerChoise
if (rock.includes(computerChoice)) {
  computerChoice = "Камінь";
} else if (scissors.includes(computerChoice)) {
  computerChoice = "Ножиці";
} else {
  computerChoice = "Папір";
}

var userChoise = prompt("Камінь, Ножиці, Папір?");

// перевірка на випадок коли не введено дані користувачем і нажато на ОК або ВІДМІНА
while (userChoise === null || userChoise === "") {
  userChoise = prompt("Введення відсутні. Камінь, Ножиці, Папір?");
}
console.log(`Ваш вибір: ${userChoise}`);
// перебір userChoise і переприсвоєння значень
switch (userChoise) {
  case "Камінь":
    userChoise = "Камінь";
    break;
  case "Ножиці":
    userChoise = "Ножиці";
    break;
  case "Папір":
    userChoise = "Папір";
    break;
  default:
    document.write("Помилка, введення не вірне");
}
console.log(`Комп: ${computerChoice}; Мій вибір: ${userChoise}`);

//порівняння виборів
function checkOnClick() {
  if (computerChoice == userChoise) {
    document.write(
      `<div style='color:orange; text-align:center;'><h1>НІЧИЯ!</h1>Вибори співпали: <br> ${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Камінь" && userChoise == "Ножиці") {
    document.write(
      `<div style='color:black; text-align:center;'><h1>Ви програли!</h1><br>Камінь ламає ножиці:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Камінь" && userChoise == "Папір") {
    document.write(
      `<div style='color:green; text-align:center;'><h1>УРА! Ви виграли!</h1><br>Камінь накриває папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Ножиці" && userChoise == "Камінь") {
    document.write(
      `<div style='color:green; text-align:center;'><h1>УРА! Ви виграли!</h1><br>Камінь ламає ножиці:<br>${computerChoice} \& ${userChoise}<div>`
    );
  } else if (computerChoice == "Ножиці" && userChoise == "Папір") {
    document.write(
      `<div style='color:black; text-align:center;'><h1>Ви програли!</h1><br>Ножиці ріжуть папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Папір" && userChoise == "Камінь") {
    document.write(
      `<div style='color:black; text-align:center;'><h1>Ви програли!</h1><br>Камінь накриває папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  } else if (computerChoice == "Папір" && userChoise == "Ножиці") {
    document.write(
      `<div style='color:green; text-align:center;'><h1>УРА! Ви виграли!</h1><br>Ножиці ріжуть папір:<br>${computerChoice} \& ${userChoise}</div>`
    );
  }
}
checkOnClick();
console.log("****************************************");

// 4. Напишите функцию, которая возвращает n-е число Фибоначчи. Для решения используйте цикл. 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946...
var num1 = 0;
var num2 = 1;
var num3 = 1;
var prevNum = num3 + num2;
var nextNum;

var maxNum = Infinity;
var position = prompt(
  "Введіть позицію числа в ряді Фібоначчі, починаючи від 1: "
);

// перевірка на випадок без введення і натиснуто ОК або ВІДМІНА
while (position === null || position === "") {
  position = prompt(
    "Дані не введено. Введіть позицію числа в ряді Фібоначчі: "
  );
}

// перетворення в числовий тип і перевірка на число
var numPosition = +position;

if (isNaN(numPosition) || numPosition <= 0) {
  document.write("Ви ввели не коректне число. ПОМИЛКА");
} else {
  getNumFromFibonachi(numPosition);
}

// функція
function getNumFromFibonachi(numPosition) {
  var result = [0, 1, 1, 2];
  for (var i = 1; i < maxNum; i++) {
    nextNum = prevNum + num3;
    result.push(nextNum);
    num3 = prevNum;
    prevNum = nextNum;

    if (i == numPosition) {
      break;
    }
  }
  return document.write(
    `<b>Число в ряду Фібоначчі: ${
      result[i - 1]
    }.</b><br>Введена позиція числа: ${numPosition}.<br>Масив чисел до вказаної позиції: \[${result.slice(
      0,
      numPosition
    )}\]`
  );
}
