// 1. Сделать собственные примеры методов применяемых для массивов.
var cities = ["Київ", "Харків", "Одеса", "Дніпро", "Донецьк"];
//join
var changeArr = cities.join(", ");
document.write(
  `${changeArr} - це п'ятірка найбільших міст України за неселенням.`
);

var changeArr2 = cities.join("<br> *");
document.write(`<br><br> * ${changeArr2} <br>`);

document.write(`<br>П'ятірка найбільших міст України за неселенням:`);
document.write("<ul style='list-style:square'><li>");
var changeArr3 = cities.join("</li><li>");
document.write(changeArr3);
document.write("</li> </ul>");

//concat
var cities1 = ["Київ", "Харків", "Одеса", "Дніпро", "Донецьк"];
var cities2 = ["Запоріжжя", "Львів"];

var citiesUkr = cities1.concat(cities2);
console.log(citiesUkr);
document.write(`<br>${citiesUkr.join(", ")} - міста України.`);

var userName = [10, 20];
var arrayNum = [30, 40, 50];

var result = [].concat(userName, arrayNum, 6, 7, 8, 9, 100);
console.log("Об'єднані масиви в один: ");
console.log(result);

console.log("***");

//reverse + sort
var revResult = result.reverse();
console.log(revResult); //reverse масив

var toMinNum = revResult.sort(function (a, b) {
  return b - a;
});
console.log("Відсортований масив по спаданню: " + toMinNum);

var toMaxNum = toMinNum.reverse();
console.log("Відсортований масив по зростанню: " + toMaxNum);

console.log("***");

//slice
var capital = cities1.slice(0, 1);
console.log(capital + " - столиця України.");

//дії з масивом result = [6, 7, 8, 9, 10, 20, 30, 40, 50, 100];
var resultNum1 = result.slice(4);
var resultNum2 = result.slice(2, 5);
console.log(resultNum1);
console.log(resultNum2);

var hundred = result.slice(result.length - 1);
console.log(hundred);
console.log(result.slice(-1));

console.log("***");

//splice
var numbers = [6, 7, 8, 9, 10, 20, 30, 40, 50, 100];

var partArray = numbers.slice(0, 4);
console.log("Новий масив без десяткових: " + partArray);

var decimal = partArray.splice(0, 0, 1, 2, 3, 4, 5);
console.log("Доповнений масив: ");
console.log(partArray);
console.log("Видалено 0 елементів, тому масив пустий: ");
console.log(decimal);

console.log("***");

//push and pop
var countries1 = ["Italy", "France", "Poland"];
console.log(`Початковий масив: ${countries1}`);

var newLengthArray = countries1.push("Ukraine", "England", "Greece");

console.log(`Доповнений масив: ${countries1}`);
console.log(`Довжина доповненого масиву: ${newLengthArray}`);

var delCountry = countries1.pop();
console.log(`Зменшений на один елемент масив: ${countries1}`);
console.log(`Видалений елемент з кінця масиву: ${delCountry}`);

console.log("***");

//shift and unshift
var days = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця"];
var weekLength = days.unshift("Неділя", "Субота");

console.log("Дні тижня " + days);
console.log(`Кількість днів у тижні: ${weekLength}`);

var weekend1 = days.shift();
var weekend2 = days.shift();

console.log(weekend1);
console.log(weekend2);
console.log(days);

var newWeekLength = days.push(weekend2, weekend1);
console.log(`Тиждень знову має кількість днів: ${newWeekLength}`);
console.log(`Додаємо вихідні в кінець масиву: ${days}`);

// 2. Создать функцию для нахождения минимального и максимального элемента массива getMinAndMax(array).Результат должен выводиться в консоль.

//через цикл for
var arr = [2, 5, 8, 1, 6, 9, 3];
var minNum = arr[0];
var maxNum = arr[1];

for (let i = 0; i < arr.length; i++) {
  if (arr[i] < minNum) {
    minNum = arr[i];
  } else if (arr[i] > maxNum) {
    maxNum = arr[i];
  }
}

console.log(`Min: ${minNum}`);
console.log(`Max: ${maxNum}`);

//спробувала через функцію
var arr = [2, 5, 8, 1, 6, 9, 3];
var arr1 = [-100, 5, 8747, 174, 674, 9747, 3747747];

function getMinAndMax(array) {
  let min = array[0];
  let max = array[0];

  for (const item of array) {
    if (item < min) {
      min = item;
    }
    if (item > max) {
      max = item;
    }
  }
  return `Min value is: ${min} and max value is ${max}`;
}
console.log(getMinAndMax(arr));
console.log(getMinAndMax(arr1));

// 3. Определить элементы массива и вывести их значения, индексы которых лежит в указанном пределе. Предел вводит пользователь.
let array = [1, 5, 8, 3, 5, 2, 4, 6, 9];

var first = prompt(`Введіть start від 0 до ${array.length - 1}`);
var second = prompt(`Введіть finish від 0 до ${array.length - 1}`);

while (first === null || second === null || first === "" || second === "") {
  //перевірка на випадок коли натиснуто відміну або ок
  first = prompt(
    `Дані не введено. Введіть заново first від 0 до ${array.length - 1}`
  );
  second = prompt(`І заново second від 0 до ${array.length - 1}`);
}
while (
  +first < 0 ||
  +first > array.length ||
  +second < 0 ||
  +second > array.length
) {
  //перевірка на випадок коли введені числа не входять у вказаний проміжок
  first = prompt(`Введіть нове число start від 0 до ${array.length - 1}`);
  second = prompt(`Введіть нове число finish від 0 до ${array.length - 1}`);
}
while (+first > +second) {
  //перевірка на випадок коли початкове більше за кінцеве значення
  first = prompt(
    `Початкове значення більше ніж кінцеве.,/\nВведіть new start від 0 до ${
      array.length - 1
    }`
  );
  second = prompt(`Введіть нове число new finish від 0 до ${array.length - 1}`);
}

//переводимо в числовий тип введені дані і присвоюємо змінним
var start = +first;
var finish = +second;

//додаємо введені дані в масив для перевірки на тип
var arr = [];
arr.push(start, finish);
console.log(arr);

if (arr.includes(NaN)) {
  document.write(
    "Ви ввели не числові значення, вивести дані не можливо. Оновіть сторінку"
  );
} else {
  var newArray = array.slice(start, finish + 1);
  console.log(`Новий масив: ${newArray}`);
  document.write(
    `Новий масив значень в проміжку від індексу \[${start}\] до індексу \[${finish}\] включно: ${newArray}`
  );
}

// 4. Отсортировать массив на 10 чисел в порядке возростания. Значения принимаются от пользователя. Сортировка осушествляется методом sort().
var string = prompt("Введіть десять чисел");
var arr = [];

while (string === null || string === "") {
  //перевірка на випадок коли натиснуто відміну або ок
  string = prompt("Дані не було введено. Введіть десять чисел");
}
while (string !== null && string !== "") {
  arr.push(+string);
  console.log(arr);

  if (arr.length >= 10) {
    break;
  }
  string = prompt("Введіть наступне число");
}

for (let i = 0; i < arr.length; i++) {
  arr.sort(function (a, b) {
    return a - b;
  });
}
console.log(`Відсортований масив в порядку зростання: ${arr}`);

// 5. Создать программу, выполняющую вставку элемента (вставка от пользователя) в массив после элемента, значение которого укажет пользователь.
let array3 = [1, 5, 8, 3, 5, 2, 4, 6, 9];

var first = prompt("Введіть значення, яке ви хочете додати");

while (first === null || first === "") {
  //перевірка на випадок коли натиснуто відміну або ок для першого значення
  first = prompt("Значення не введено. Введіть заново значення для first");
}

var second = prompt(`Введіть число від 0 до ${array3.length - 1}`);

while (second === "" || second === null) {
  second = prompt(`Введіть число від 0 до ${array3.length - 1}`);
}

second = +second;

while (Number.isNaN(second) || second > array3.length - 1 || second < 0) {
  second = +prompt(`Введіть число від 0 до ${array3.length - 1}`);
}

console.log(first);
console.log(second);

array.splice(second, 0, first);
console.log(array3);

// 6. Необходимо получить предпоследний элемент из массива month и вывести его на экран.(різними способами)
// var month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

var month = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];
//спосіб 1
document.write(`Останній елемент масиву: ${month[month.length - 2]}`);
document.write("<br>");

//спосіб 2
var a = month.length - 1;
var b = month.length - 2;
var elem = month.slice(b, a);

document.write(`Спосіб 2: ${elem}`);
console.log(elem);
console.log(month);
document.write("<br>");

// спосіб 3
var elem2 = month.splice(length - 2, 1, "Ноябрь");
console.log(elem2);
console.log(month);
document.write(`Спосіб 3: ${elem2}`);

document.write("<br>");

// спосіб 4
var elem2 = month.splice(0, b);
elem2 = elem2.concat(month.splice(month.length - 1));
console.log(elem2);
console.log(month);
document.write(`Спосіб 4: ${month}`);

// 7. Создать два произвольных массива: один массив - любимая спортивная команда / модели автомобилей / города, во втором - количество игроков в команде / объем двигателя / количество жителей в городе. Объединить данные из двух массивов и вывести их в одну строку.

var cars = ["Audi", "Mercedes", "Fiat", "Ford", "Porsche"];
var value = [2, 5, 3, 6, 8];
var newArray = [];

for (let i = 0; i < cars.length; i++) {
  if (cars.length != value.length) {
    console.log("Довжини масивів не однакові");
    break;
  }

  newArray.push(cars[i]);
  newArray.push(value[i]);
}
console.log(newArray);
document.write(newArray.join(" * "));
